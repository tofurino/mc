# Minecraft 1.19 quality of life (QOL) client side mods

- [Xaero's Minimap](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap)

Minimap mod + waypoints

- [Xaero's World Map](https://www.curseforge.com/minecraft/mc-mods/xaeros-world-map)

Press M for map!

- [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api)

Library for other mods (required for most of the mods in this list i think)

- [Sodium](https://www.curseforge.com/minecraft/mc-mods/sodium)

Client performance

- [Iris Shaders](https://www.curseforge.com/minecraft/mc-mods/irisshaders)

For optifine shaders replacement (requires sodium)

## Waitlist

- [Fast Leaf Decay](https://www.curseforge.com/minecraft/mc-mods/fast-leaf-decay)

It's in the name (wait what is a forge mod doing here????)

- [Lithium (server side)](https://www.curseforge.com/minecraft/mc-mods/lithium)

Improved performance

- [Fabric Homes (server side)](https://www.curseforge.com/minecraft/mc-mods/fabrichomes)

/HOME /HOME /HOME /HOME /HOME /HOME /HOME /HOME

# Installation instructions (PolyMC)

1. Grab mod .jar files (for FABRIC) from links above or from the repo
2. Create an instance for Minecraft 1.19 (New release POG!)
3. Edit instance > Version tab > Install Fabric (best version should already be selected)
4. Mods tab > drag downloaded .jar files in
